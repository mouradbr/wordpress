<?php
/**
 * Genesis Sample.
 *
 * This file adds the landing page template to the Genesis Sample Theme.
 *
 * Template Name: Porte_folio
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    https://www.studiopress.com/
 */

add_action('genesis_meta', 'clean_model');
/**
 * Add widget support for homepage. If no widgets active, display the default loop.
 *
 */
function clean_model()
{ 
    //* Force full width content layout
    add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');
    //* Remove primary navigation
    //remove_action('genesis_before_content_sidebar_wrap', 'genesis_do_nav');
    //* Remove breadcrumbs
    remove_action('genesis_before_loop', 'genesis_do_breadcrumbs');
    //* Remove the default Genesis loop
    remove_action('genesis_loop', 'genesis_do_loop');
    remove_action('genesis_entry_content', 'genesis_do_post_content');
   
}
add_action('genesis_before_loop','show_porte');
function show_porte(){
    ?>
    <div class="container">
    <div class="row">
    <?php
    $param = array(
        
   );
               $portfolios = pods('portfolio', $param );
               if ( $portfolios->total() > 0 ) {
               while ($portfolios->fetch()) {
               //var_dump( $portfolios->field('image1'));
               echo $portfolios->field('post_thumbnail_url.full');
               $id= $portfolios->field('id');
               
               $url_image=pods_image_url ( $portfolios->field('image1'), 'thumbnail' );
                 $url_image1=pods_image_url ( $portfolios->field('image2'), 'thumbnail' );
                 $url_image2=pods_image_url ( $portfolios->field('image3'), 'thumbnail' );
                 $url_image3=pods_image_url ( $portfolios->field('image4'), 'thumbnail');
                 ?>
                
               <div class="row-lg-3 azerty">
               <a  href="<?php the_permalink($id); ?>"><?php  echo "<img  src='".$url_image."' >" ;?></a> 
               <a  href="<?php the_permalink($id); ?>"><?php echo "<img src='".$url_image1."' >" ;?>
               <a  href="<?php the_permalink($id); ?>"><?php echo "<img src='".$url_image2."' >" ;?>
               <a  href="<?php the_permalink($id); ?>"><?php echo "<img src='".$url_image3."' >" ;?></div>
    </div>
    </div>

            <?php } } } 
   genesis();
?>