<?php
/**

 * Template Name: Boutique

 */
add_action('genesis_meta', 'clean_model');
/**
 * Add widget support for homepage. If no widgets active, display the default loop.
 *
 */
function clean_model()
{ 
    //* Force full width content layout
    add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');
    //* Remove primary navigation
    //remove_action('genesis_before_content_sidebar_wrap', 'genesis_do_nav');
    //* Remove breadcrumbs
    remove_action('genesis_before_loop', 'genesis_do_breadcrumbs');
    //* Remove the default Genesis loop
    remove_action('genesis_loop', 'genesis_do_loop');
    remove_action('genesis_entry_content', 'genesis_do_post_content');
   
}
genesis();
