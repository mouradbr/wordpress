<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'mourad.test' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '?H-iy6[p>=o^{y,LEHKeHLnU] IEvH>iR^!vK,}<&t& JJ~~]{E8`=UlB[%HC:Ad' );
define( 'SECURE_AUTH_KEY',  '/^uT`-*,Cvh%qv0JNuQVwLjqz.n;UCA_udha.%Z/*74PVv#+}aUj#-+&,IFm^o1!' );
define( 'LOGGED_IN_KEY',    '}|3jHH`vz:%}@O1QEK&=DWyU-)!Mn8D}!$I{_G#$=%;rV7)/ Ep_zRP^ !5|-k!w' );
define( 'NONCE_KEY',        '13bN:A|T-7.2O8Bd.09 Oi^f~7CH?DxK`bwt0#mx7rB1xZr@P6rDB|bR3TuhO8Ra' );
define( 'AUTH_SALT',        'xv#qt[9cMwUm9E)r7d8Z -Kn&VvCflG7d4WGr+*.C*AO+5ebD+swL:6p:p=qcWyj' );
define( 'SECURE_AUTH_SALT', ',Y,r7mo~#5kC>~Q m_^dYQWUf5)e9c0W?<@K.9)~s0+=k>V=F/*EMIm/ (kSMUEG' );
define( 'LOGGED_IN_SALT',   '+x@:#PSXQ/cxDC3<X,X&z3:nm[T-@^Uhhp.{0?#9ok_Wv-Abk&dCQniHaWZL~5a{' );
define( 'NONCE_SALT',       'i#JM4<x|wxRf`r_@.A0:qj7:h&i;5+3x$%F!1vTYGBE}3^<uO$&nfMFbwG4ouz[%' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp2_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

define ('WP_ALLOW_REPAIR',true);
